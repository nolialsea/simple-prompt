require('dotenv').config()
const axios = require('axios')
const conf = require('./conf.json')
const vocab = require('./vocab.json')
const vocabIndexed = require('./vocabIndexed.json')
const http = require('http')
const fs = require('fs')
const {Server} = require("socket.io");
const server = http.createServer((req, res) => {
    if (req.url === "/") {
        res.writeHead(200, {'content-type': 'text/html'})
        fs.createReadStream('simplePrompt.html').pipe(res)
    }else if(req.url === "/simple-tokens"){
        res.writeHead(200, {'content-type': 'text/html'})
        fs.createReadStream('simpleTokens.html').pipe(res)
    }
})
const io = new Server(server);

io.on('connection', (socket) => {
    socket.on('prompt', async (prompt, opts) => {
        opts.number_generated_tokens = Math.min(100, opts.number_generated_tokens ?? 10)

        try {
            const answer = (await sendPrompt(
                prompt,
                opts
            ))

            socket.emit('prompt', answer)
        } catch (e) {
            console.log("Error while fetching prompt...")
        }
    });

    socket.on('tokens', async (prompt, id) => {
        try {
            const tokensPrompt = (await getTokens(prompt)).map((token) => [
                token, vocabIndexed[token]
            ])
            socket.emit('tokens', tokensPrompt, id)
        }catch{

        }
    });

    socket.on('string-tokens', async (prompt, id, caseSensitive = false) => {
        try {
            const tokenString = (await getTokens(prompt))
                .map((token) => vocabIndexed[token])
                .join('')
            const tokensString = (await getStringTokens(tokenString, caseSensitive))
                .map((token) => [
                    token, vocab[token]
                ])
            socket.emit('string-tokens', tokensString, id)
        } catch {

        }
    });
});

server.listen(process.env.PORT ? parseInt(process.env.PORT) : conf.port)

async function getTokens(prompt) {
    if (prompt)
        return await sendPostRequest(conf.apiUrlToken, {prompt})
}

async function getStringTokens(string, caseSensitive = false) {
    if (string)
        return await sendPostRequest(conf.apiUrlStringToken, {string, case_sensitive: caseSensitive})
}

async function sendPrompt(prompt, opts) {
    opts.prompt = prompt

    if (opts.prompt)
        return await sendPostRequest(conf.apiUrl, opts)
}

function sendPostRequest(url, data) {
    return new Promise((accept, reject) => {
        axios.post(url, data)
            .then((result) => {
                const answer = result.data
                if (answer) {
                    accept(answer)
                } else {
                    reject()
                }
            })
            .catch(() => {
                reject()
            })
    })
}