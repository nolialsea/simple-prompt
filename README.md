# Simple Prompt

Simple Prompt tool that uses gpt-neo 2.7B horni-ln model  
Live version should be running [HERE](http://86.193.242.125:3003/), if it's not, my old 1080ti is probably getting some
rest

# Tips & Tricks

- Don't put a space at the end of your story/prompt
    - AI prefers to generate words with leading space
    - However it's safe to end the story/prompt with a newline
- Stories longer than 2048 tokens get cropped server side, only the last 2048 tokens are kept
- The weird `Ġ` and `Ċ` symbols in tokens represent respectively a space and a newline
    - Most tokens are prefixed with a space, it's not a bug
- To be tested further:
    - `***` line results in more lewd and sensual type of narrative, feels like it's latching onto quite explicit
      sex-scenes
    - `---` line leads to either clothes cutting, or more commonly the taking off clothes part getting skipped
    - `⁂` line results in narrative and getting lewd within like 40 tokens, no sensual vibe but rather and matter-of-fact one

# Prerequisites

Is project uses my [Horni API](https://gitlab.com/nolialsea/horni-api) to generate the AI results  
You have to install and run it in order for simple prompt to work 

# Install & Run (requires AI API to run too)

`npm i` installs requirements  
`node ./simplePromptServer.js` starts the server
